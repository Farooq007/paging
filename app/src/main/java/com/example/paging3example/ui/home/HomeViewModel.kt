package com.example.paging3example.ui.home

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.paging3example.data.model.Home
import com.example.paging3example.data.repository.HomeRepository
import com.example.paging3example.ui.main.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val homeRepository: HomeRepository) :
    BaseViewModel() {
    private lateinit var _homeFlow: Flow<PagingData<Home>>

    val homeFlow: Flow<PagingData<Home>>
        get() = _homeFlow

    init {
        getAllHomeData()
    }

    private fun getAllHomeData() = launchPagingAsync({
        homeRepository.getHomeData().cachedIn( viewModelScope)
    }, {
        _homeFlow = it
    })
}