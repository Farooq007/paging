package com.example.paging3example.ui.home


import androidx.fragment.app.viewModels
import androidx.paging.LoadState
import com.example.paging3example.R
import com.example.paging3example.databinding.FragmentHomeBinding
import com.example.paging3example.ui.HomeAdapter
import com.example.paging3example.ui.main.BaseFragment
import com.example.paging3example.utils.PagingLoadStateAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {
    private val homeViewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var homeAdapter: HomeAdapter

    override val layoutId: Int = R.layout.fragment_home

    override fun getVM(): HomeViewModel = homeViewModel

    override fun bindVM(binding: FragmentHomeBinding, vm: HomeViewModel) =
        with(binding) {
            with(homeAdapter) {
                rvCharacters.apply {
                    postponeEnterTransition()
                    viewTreeObserver.addOnPreDrawListener {
                        startPostponedEnterTransition()
                        true
                    }
                }
                rvCharacters.adapter = withLoadStateHeaderAndFooter(
                    header = PagingLoadStateAdapter(this),
                    footer = PagingLoadStateAdapter(this)
                )

                swipeRefresh.setOnRefreshListener { refresh() }

                with(vm) {
                    launchOnLifecycleScope {
                        homeFlow.collectLatest { submitData(it) }
                    }
                    launchOnLifecycleScope {
                        loadStateFlow.collectLatest {
                            swipeRefresh.isRefreshing = it.refresh is LoadState.Loading
                        }
                    }
                }
            }
        }
}