package com.example.paging3example.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.paging3example.data.model.Home
import com.example.paging3example.databinding.ItemHomeBinding
import javax.inject.Inject

class HomeAdapter @Inject constructor() :
    PagingDataAdapter<Home, HomeAdapter.HomeViewHolder>(HomeComparator) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        HomeViewHolder(
            ItemHomeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class HomeViewHolder(private val binding: ItemHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Home) = with(binding) {
            ViewCompat.setTransitionName(binding.ivAvatar, "avatar_${item.id}")
            ViewCompat.setTransitionName(binding.tvName, "name_${item.id}")
            home = item
        }
    }

    object HomeComparator : DiffUtil.ItemCallback<Home>() {
        override fun areItemsTheSame(oldItem: Home, newItem: Home) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Home, newItem: Home) =
            oldItem == newItem
    }
}