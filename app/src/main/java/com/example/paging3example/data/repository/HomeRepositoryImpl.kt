package com.example.paging3example.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.paging3example.data.model.Home
import com.example.paging3example.data.paging.datasource.HomePagingDataSource
import com.example.paging3example.data.service.HomeApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class HomeRepositoryImpl @Inject constructor(private val service: HomeApi) : HomeRepository {

    override suspend fun getHomeData(): Flow<PagingData<Home>> = Pager(
        config = PagingConfig(pageSize = 20, prefetchDistance = 2),
        pagingSourceFactory = { HomePagingDataSource(service) }
    ).flow
}