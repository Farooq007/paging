package com.example.paging3example.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Home (val id: Int,
                 val name: String,
                 val image: String,
                 val url: String) :Parcelable