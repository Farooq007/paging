package com.example.paging3example.data.paging.datasource

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.paging3example.data.model.Home
import com.example.paging3example.data.service.HomeApi

class HomePagingDataSource( private val service: HomeApi) : PagingSource<Int, Home>(){
    override fun getRefreshKey(state: PagingState<Int, Home>): Int? = 1
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Home> {
        val pageNumber = params.key ?: 1
        return try {
            val response = service.getAllHomeData(pageNumber)
            val pagedResponse = response.body()
            val data = pagedResponse?.results

            var nextPageNumber: Int? = null
            if (pagedResponse?.pageInfo?.next != null) {
                val uri = Uri.parse(pagedResponse.pageInfo.next)
                val nextPageQuery = uri.getQueryParameter("page")
                nextPageNumber = nextPageQuery?.toInt()
            }

            LoadResult.Page(
                data = data.orEmpty(),
                prevKey = null,
                nextKey = nextPageNumber
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}