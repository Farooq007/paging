package com.example.paging3example.data.repository

import androidx.paging.PagingData
import com.example.paging3example.data.model.Home
import kotlinx.coroutines.flow.Flow

interface HomeRepository {
    suspend fun getHomeData(): Flow<PagingData<Home>>
}