package com.example.paging3example.data.service

import com.example.paging3example.data.model.Home
import com.example.paging3example.data.model.PagedResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeApi {
    @GET("character/")
    suspend fun getAllHomeData(@Query("page") page: Int): Response<PagedResponse<Home>>
}